﻿using _50linq.DataClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _50linq
{
    class Program
    {
        static void Main(string[] args)
        {
            UserRepository userRepository = new UserRepository();
            AccountRepository accountRepository = new AccountRepository();
            HistoryRepository historyRepository = new HistoryRepository();

            List < User > users = userRepository.GetAll();

            //1.Вывод информации о заданном аккаунте по логину и парол (Linq внутри FindByLoginPassword)
            Console.WriteLine("Вывод информации о заданном аккаунте по логину и паролю");
            User user = userRepository.FindByLoginPassword("luda", "adul");
            Console.WriteLine($"Id = {user.Id}, Name = {user.Name}, SecondName = {user.SecondName}" );
            Console.WriteLine();

            //2.Вывод данных о всех счетах заданного пользователя (Linq внутри FindByUser)
            Console.WriteLine("Вывод данных о всех счетах заданного пользователя");
            var accounts = accountRepository.FindByUser(user);
            accounts.Select(a => { Console.WriteLine($"Id = {a.Id}, DataOpen = {a.DataOpen}, Summ = {a.Summ}"); return a; }).Count();
            Console.WriteLine();

            //3.Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
            Console.WriteLine("Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту");
            var q = from acc in accounts
                    from hist in historyRepository.FindByAccount(acc)
                    select 
                    new
                    {
                        AccountId = acc.Id, AccountDataOpen = acc.DataOpen, AccountSumm = acc.Summ,
                        HistoryId = hist.Id, OperDate = hist.OperDate, OperType = hist.Type, OperSumm = hist.Summ,
                    };

            int lastAccountId = int.MinValue;
            q.Select(a => 
            {
                if (lastAccountId != a.AccountId)
                {
                    Console.WriteLine($"AccountId = {a.AccountId}, AccountDataOpen = {a.AccountDataOpen}, AccountSumm = {a.AccountSumm}");
                    lastAccountId = a.AccountId;
                }
                Console.WriteLine($"     HistoryId = {a.HistoryId}, OperDate = {a.OperDate}, OperType = {a.OperType}, OperSumm = {a.OperSumm}");
                return a;
            }).Count();
            Console.WriteLine();

            //4.Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
            Console.WriteLine("Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
            var q4 = from hist in historyRepository.GetAll()
                     where hist.Type == History.TypeOperation.In
                     join acc in accountRepository.GetAll() on hist.AccountId equals acc.Id
                     join usr in userRepository.GetAll() on acc.OwnerId equals usr.Id
                     select new { hist, usr};

            foreach (var el in q4)
            {
                Console.WriteLine($"OperDate = {el.hist.OperDate}, Summ = {el.hist.Summ}, Name = {el.usr.Name}, SecondName = {el.usr.SecondName}");
            }

            Console.WriteLine();

            //5.Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой)
            Console.WriteLine("Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой)");
            Console.Write("Введите N : ");
            decimal N = Convert.ToDecimal( Console.ReadLine());

            var q5 = from usr in userRepository.GetAll()
                     join acc in accountRepository.GetAll() on usr.Id equals acc.OwnerId
                     where acc.Summ > N
                     select usr;

            foreach (var el in q5)
            {
                Console.WriteLine($"Id = {el.Id}, Name = {el.Name}, SecondName = {el.SecondName}");
            }

            Console.ReadKey();
        }
    }
}
