﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _50linq.DataClass
{
    public class AccountRepository
    {
        List<Account> accounts;
        public AccountRepository()
        {
            CultureInfo cult = CultureInfo.CreateSpecificCulture("ru-RU");
            accounts = new List<Account>()
            {
                new Account() { Id = 1, OwnerId = 2, Summ = 100_000_000,  DataOpen = DateTime.Parse("01.10.2010", cult) },
                new Account() { Id = 2, OwnerId = 5, Summ = 4_000_000,  DataOpen = DateTime.Parse("01.04.2015", cult) },
                new Account() { Id = 3, OwnerId = 5, Summ = 5_000_000,  DataOpen = DateTime.Parse("01.04.2015", cult) },
            };
        }

        public List<Account> FindByUser(User user)
        {
            return (from ac in accounts where ac.OwnerId == user.Id select ac).ToList();
        }

        public List<Account> GetAll()
        {
            return accounts;
        }
    }
}
