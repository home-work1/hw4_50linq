﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _50linq.DataClass
{
    public class History
    {
        public enum TypeOperation { In,Out }

        public History() { }

        public History(Account account)
        {
            AccountId = account.Id;
        }

        public int Id;

        /// <summary>
        /// Дата операции
        /// </summary>
        public DateTime OperDate { get; set; }

        /// <summary>
        /// Тип операции
        /// </summary>
        public TypeOperation Type { get; set; }

        /// <summary>
        /// Cумма,
        /// </summary>
        public decimal Summ { get; set; }

        /// <summary>
        /// Id счёта
        /// </summary>
        public int AccountId { get; set; }

    }
}
