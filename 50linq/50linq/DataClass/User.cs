﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _50linq.DataClass
{
    public class User
    {
        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Фамилия 
        /// </summary>
        public string SecondName { get; set; }

        //отчество, //телефон, //данные паспорта, //дата регистрации, 

        /// <summary>
        /// Логин 
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }
    }
}
