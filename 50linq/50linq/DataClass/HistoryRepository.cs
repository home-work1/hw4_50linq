﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _50linq.DataClass
{
    public class HistoryRepository
    {
        private List<History> historys;

        public HistoryRepository()
        {
            CultureInfo cult = CultureInfo.CreateSpecificCulture("ru-RU");

            historys = new List<History>()
            {
                new History(){Id = 1, AccountId = 3,Summ = 2_000_000, Type = History.TypeOperation.In,  OperDate =  DateTime.Parse("01.10.2016", cult)  },
                new History(){Id = 2, AccountId = 3,Summ = 3_000_000, Type = History.TypeOperation.In,  OperDate =  DateTime.Parse("01.10.2014", cult)  },
                new History(){Id = 3, AccountId = 3,Summ = 1_000_000, Type = History.TypeOperation.Out, OperDate =  DateTime.Parse("01.10.2015", cult)  },
                new History(){Id = 4, AccountId = 3,Summ = 4_000_000, Type = History.TypeOperation.Out, OperDate =  DateTime.Parse("01.10.2012", cult)  },
                new History(){Id = 9, AccountId = 3,Summ = 5_000_000, Type = History.TypeOperation.In,  OperDate =  DateTime.Parse("01.10.2018", cult)  },

                new History(){Id = 5, AccountId = 2,Summ = 3_000_000, Type = History.TypeOperation.In,  OperDate =  DateTime.Parse("01.10.2014", cult)  },
                new History(){Id = 6, AccountId = 2,Summ = 2_000_000, Type = History.TypeOperation.Out, OperDate =  DateTime.Parse("01.10.2013", cult)  },
                new History(){Id = 7, AccountId = 2,Summ = 1_000_000, Type = History.TypeOperation.Out, OperDate =  DateTime.Parse("01.10.2012", cult)  },
                new History(){Id = 8, AccountId = 2,Summ = 4_000_000, Type = History.TypeOperation.In,  OperDate =  DateTime.Parse("01.10.2014", cult)  },

                new History(){Id = 10, AccountId = 1,Summ = 60_000_000, Type = History.TypeOperation.In, OperDate =  DateTime.Parse("01.10.2013", cult)  },
                new History(){Id = 11, AccountId = 1,Summ = 40_000_000, Type = History.TypeOperation.In, OperDate =  DateTime.Parse("01.10.2014", cult)  },

            };
        }

        public List<History> FindByAccount(Account account)
        {
            return historys.Where(a => a.AccountId == account.Id).ToList();
        }

        public List<History> GetAll()
        {
            return historys;
        }

    }
}
