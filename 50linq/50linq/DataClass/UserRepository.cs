﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _50linq.DataClass
{
    public class UserRepository
    {
        private List<User> users;

        public UserRepository()
        {
            users = new List<User>();
            users.Add(new User() { Id = 1, Name = "Коля", SecondName = "Борисов", Login = "boris", Password = "sirob"});
            users.Add(new User() { Id = 2, Name = "Ксюша",  SecondName = "Боткина", Login = "olia",  Password = "ailo" });
            users.Add(new User() { Id = 3, Name = "Лоля", SecondName = "Марус",   Login = "loli",  Password = "ilol" });
            users.Add(new User() { Id = 4, Name = "Толя", SecondName = "Толянов", Login = "tolik", Password = "kilot"});
            users.Add(new User() { Id = 5, Name = "Люда", SecondName = "Антошкина",   Login = "luda",  Password = "adul" });
        }


        public User FindByLoginPassword(string login,string password)
        {
            return (from u in users where u.Login == login && u.Password == password select u).FirstOrDefault();
        }

        public List<User> GetAll()
        {
            return users;
        }
    }
}
