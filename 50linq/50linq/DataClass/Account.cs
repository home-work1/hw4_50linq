﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _50linq.DataClass
{
    public class Account
    {
        public int Id { get; set; }

        /// <summary>
        /// Дата открытия счёта
        /// </summary>        
        public DateTime DataOpen { get; set; }

        /// <summary>
        /// Cумма на счёте
        /// </summary>
        public decimal Summ { get; set; }

        /// <summary>
        /// Id владельца
        /// </summary>
        public decimal OwnerId { get; set; }
    }
}
